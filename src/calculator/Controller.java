package calculator;

import calculator.model.CalcModel;
import calculator.view.ViewCalc;

public class Controller {
	
	private static Controller instance = new Controller();
  
	ViewCalc viewCalc = ViewCalc.getInstance();
	CalcModel model = CalcModel.getInstance();
	
	private String history = "";
		
	private Controller() {
	}
	
	public static Controller getInstance() {
		return instance;
	}
	
	public String[] getOperations() {
		
		Object[] arr = model.getCalcMap().keySet().toArray();
		String[] arrOperationsStr = new String[arr.length];
		
		for (int i = 0; i < arr.length; i++) {
			arrOperationsStr[i] = (String) arr[i];					
		}
		return arrOperationsStr;
	}
	
	public double operate(String operator, double numA, double numB) {
		
		return model.getCalcMap().get(operator).operate(numA, numB);
	}
	
	public void makeResultAndAddToHistory (double numA,double numB,String operator) {
		
		double result = operate(operator, numA, numB);				
		String historyString = numA + " " + operator + " " + numB + " = " + result + "\n";
		history += historyString;	
		
		viewCalc.getResultField().setText(Double.toString(result));
		viewCalc.getHistoryText().setText(history);
		
	}
	
	
}
