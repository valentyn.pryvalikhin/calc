package calculator;

import calculator.model.CalcModel;
import calculator.view.ViewCalc;

public class App {
	
	public static void main(String[] args) {
		CalcModel calcModel = CalcModel.getInstance();
		calcModel.initCalcMap();
		ViewCalc viewCalc = ViewCalc.getInstance();		
		Controller controller = Controller.getInstance();
		
		viewCalc.callView(controller);
	}
}
