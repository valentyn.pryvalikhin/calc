package calculator.model.operations;

public class OperationSubstraction implements Operation {

	@Override
	public double operate(double numA, double numB) {

		return numA - numB;
	}

}
