package calculator.model.operations;

public interface Operation {
  public double operate (double numA, double numB);
}
