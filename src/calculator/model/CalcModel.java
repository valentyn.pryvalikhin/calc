package calculator.model;

import java.util.LinkedHashMap;
import java.util.Map;

import calculator.model.operations.Operation;
import calculator.model.operations.OperationAddition;
import calculator.model.operations.OperationDivision;
import calculator.model.operations.OperationMultiplication;
import calculator.model.operations.OperationSubstraction;

public class CalcModel {
	
	private static CalcModel instance = new CalcModel();
	
	private Map<String, Operation> calcMap = new LinkedHashMap<String , Operation>();
	  	
	private CalcModel() {
	}
	
	public static CalcModel getInstance() {
		return instance;
	}	
	
	public void initCalcMap() {
		calcMap.put("+", new OperationAddition());
		calcMap.put("-", new OperationSubstraction());
		calcMap.put("*", new OperationMultiplication());
		calcMap.put("/", new OperationDivision());
	}

	public Map<String, Operation> getCalcMap() {
		return calcMap;
	}
}
