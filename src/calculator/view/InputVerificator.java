package calculator.view;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.widgets.Text;

public class InputVerificator {
   
	protected void verifyTextForDigits(VerifyEvent event) {
				 
		    String newText = event.text;
		    String str = ((Text)event.getSource()).getText();
		    str = str + newText;
		    
		    String pattern = "([+-]? *(?:\\d+(?:\\.\\d*)?|\\.\\d+)?)|(^[+-])";
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(str);
				        
		        if (!m.matches()) {
		            event.doit = false;
		            return;
		        }		    
    	}
	
	protected void verifyCombo(VerifyEvent event, String[] operators) {
		String str = event.text;
			        
	        if (!Arrays.asList(operators).contains(str)) {
	            event.doit = false;
	            return;
	        }	
	}
	
	protected String verifySpaceMinusAndPlus(String str) {
		switch (str) {
		case "":
			return "0";
		case "+":
			return "+0";
		case "-":
			return "-0";
		default:
			return str;
		}
	}
	
	protected String comboVerify(String str, String[] operators) {
		
		if (Arrays.asList(operators).contains(str)) {
			return str;
		} else {
			return operators[0];
		}
	}
}
