package calculator.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

import calculator.Controller;

public class ViewCalc {
	
	private static ViewCalc instance = new ViewCalc();
	
	private static Text inputAField;
	private static Text inputBField;
	private static Text resultField;
	
	private boolean onFly = true;
	
	private double numA = 0;
	private double numB = 0;
	private String operator;
	private StyledText historyText;
	private ViewCalc() {
	}
	
	public static ViewCalc getInstance() {
		return instance;
	}
	
	public void callView(Controller controller) {
		
		operator = controller.getOperations()[0];
						
		InputVerificator inputVerificator = new InputVerificator();
		
		Display display = Display.getDefault();
		Shell shell = new Shell();
		shell.setMinimumSize(new Point(349, 189));
		shell.setSize(350, 305);
		shell.setText("Calculator");
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		
		TabItem tbtmCalculator = new TabItem(tabFolder, SWT.NONE);
		tbtmCalculator.setText("Calculator");
		
		Composite compositeTab = new Composite(tabFolder, SWT.NONE);
		tbtmCalculator.setControl(compositeTab);
		compositeTab.setLayout(new FillLayout(SWT.VERTICAL));
		
		Composite compositeInput = new Composite(compositeTab, SWT.NONE);
		compositeInput.setBounds(0, 0, 64, 64);
		RowLayout rl_compositeInput = new RowLayout(SWT.HORIZONTAL);
		rl_compositeInput.marginTop = 10;
		rl_compositeInput.fill = true;
		rl_compositeInput.justify = true;
		rl_compositeInput.center = true;
		compositeInput.setLayout(rl_compositeInput);
		
		inputAField = new Text(compositeInput, SWT.BORDER);
		inputAField.setText("0");
		
		inputAField.addModifyListener(new ModifyListener() {
			
			public void modifyText(ModifyEvent e) {
				numA = Double.parseDouble(inputVerificator.verifySpaceMinusAndPlus(((Text)e.getSource()).getText()));
				if (onFly) {
					controller.makeResultAndAddToHistory(numA, numB, operator);
				}
			}		
		});
		
		inputAField.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				inputVerificator.verifyTextForDigits(e);				
			}
		});
		
		inputAField.setLayoutData(new RowData(100, -1));
		
		Combo operatorCombo = new Combo(compositeInput, SWT.NONE);
		operatorCombo.setLayoutData(new RowData(30, -1));
		operatorCombo.setItems(controller.getOperations());
		operatorCombo.select(0);
		operatorCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				operator = ((Combo)e.getSource()).getText();
				
				if (onFly) {
					controller.makeResultAndAddToHistory(numA, numB, operator);
				}
			}
		});
		
		operatorCombo.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				inputVerificator.verifyCombo(e, operatorCombo.getItems());				
			}
		});		
		
		inputBField = new Text(compositeInput, SWT.BORDER);
		inputBField.setText("0");
		
		inputBField.addModifyListener(new ModifyListener() {
			
			public void modifyText(ModifyEvent e) {
				numB = Double.parseDouble(inputVerificator.verifySpaceMinusAndPlus(((Text)e.getSource()).getText()));
				if (onFly) {
					controller.makeResultAndAddToHistory(numA, numB, operator);
				}
			}
		});
		
		inputBField.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				inputVerificator.verifyTextForDigits(e);				
			}
		});
		inputBField.setLayoutData(new RowData(100, -1));
		
		Composite compositeOutput = new Composite(compositeTab, SWT.NONE);
		compositeOutput.setLayout(new FormLayout());
		
		resultField = new Text(compositeOutput, SWT.BORDER);
		resultField.setText("0.0");
		resultField.setEditable(false);
		FormData fdResultField = new FormData();
		fdResultField.bottom = new FormAttachment(100, -10);
		fdResultField.left = new FormAttachment(0, 10);
		fdResultField.right = new FormAttachment(100, -10);
		resultField.setLayoutData(fdResultField);
		
		Button btnCalculate = new Button(compositeOutput, SWT.NONE);
		btnCalculate.setEnabled(false);
		btnCalculate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controller.makeResultAndAddToHistory(numA, numB, operator);
			}
		});
		
		FormData fdBtnCalculate = new FormData();
		fdBtnCalculate.bottom = new FormAttachment(resultField, -6);
		fdBtnCalculate.right = new FormAttachment(100, -10);
		btnCalculate.setLayoutData(fdBtnCalculate);
		btnCalculate.setText("Calculate");
		
		Button btnCheckOnFly = new Button(compositeOutput, SWT.CHECK);
		FormData fdBtnCheckOnFly = new FormData();
		fdBtnCheckOnFly.bottom = new FormAttachment(resultField, -10);
		fdBtnCheckOnFly.left = new FormAttachment(0, 10);
		btnCheckOnFly.setLayoutData(fdBtnCheckOnFly);
		btnCheckOnFly.setSelection(true);
		
		btnCheckOnFly.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (((Button) e.getSource()).getSelection()) {
					onFly = true;
					btnCalculate.setEnabled(false);
				} else {
					onFly = false;
					btnCalculate.setEnabled(true);
				}
			}
		});
		btnCheckOnFly.setText("Calculate on the fly");
		
		TabItem tbtmHistory = new TabItem(tabFolder, SWT.NONE);
		tbtmHistory.setText("History");
		
		ScrolledComposite scrolledComposite = new ScrolledComposite(tabFolder, SWT.BORDER | SWT.H_SCROLL);
		tbtmHistory.setControl(scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		
		historyText = new StyledText(scrolledComposite, SWT.V_SCROLL | SWT.WRAP | SWT.BORDER);
		historyText.setEditable(false);
		scrolledComposite.setContent(historyText);
		scrolledComposite.setMinSize(historyText.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
//	private String makeResultAndAddToHistory (double numA,double numB,String operator, Controller controller) {
//	
//		double result = controller.operate(operator, numA, numB);				
//		String historyString = numA + " " + operator + " " + numB + " = " + result + "\n";
//		history += historyString;	
//		
//		return Double.toString(result);
//	}

	public static Text getResultField() {
		return resultField;
	}

	public StyledText getHistoryText() {
		return historyText;
	}	
	
	
	
}
